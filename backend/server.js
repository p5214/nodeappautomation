const express = require("express");
const cors = require("cors");
const db = require("./db");
const pool = require("./db");
const utils = require("./utils");
const app = express();

app.use(express.json());
app.use(cors("*"));

app.get("/emps", (req, resp) => {
  // create statement
  const sql = "select * from employee";

  // execute the query
  db.execute(sql, (err, data) => {
    console.log(err, data);
    resp.send(utils.createResult(err, data));
  });
});

//get emp by id
app.get("/emps/:id", (req, resp) => {
  // create statement
  const {id}=req.params
  const sql = `select * from employee where id='${id}'`;

  // execute the query
  db.execute(sql, (err, data) => {
    console.log(err, data);
    resp.send(utils.createResult(err, data));
  });
});

app.delete("/emp/:id", (req, resp) => {
  // create statement
  const { id } = req.params;
  const sql = `Delete from employee where id=${id}`;
  console.log();
  // execute the query
  db.execute(sql, (err, data) => {
    console.log(err, data);
    resp.send(utils.createResult(err, data));
  });
});

app.post("/emp/add", (req, resp) => {
  // create statement
  const { name, salary, deptid } = req.body;
  const sql = `INSERT INTO employee (name,salary,deptid) VALUES ('${name}',${salary},${deptid});`;
  console.log();
  // execute the query
  db.execute(sql, (err, data) => {
    console.log(err, data);
    resp.send(utils.createResult(err, data));
  });
});

app.patch("/emp/update", (req, resp) => {
  // create statement
  const { id, salary } = req.body;
  const sql = `Update employee SET salary = ${salary} where id= ${id}`;
  console.log();
  // execute the query
  db.execute(sql, (err, data) => {
    console.log(err, data);
    resp.send(utils.createResult(err, data));
  });
});

app.listen(4444, "0.0.0.0", () => {
  console.log("Server started on port 4444");
});

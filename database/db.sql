create table employee (
    id INT primary key auto_increment,
    name VARCHAR(100),
    salary FLOAT,
    deptid INT
);

INSERT INTO employee (name,salary,deptid) VALUES ("ram",30000,1);
INSERT INTO employee (name,salary,deptid) VALUES ("radha",40000,1);
INSERT INTO employee (name,salary,deptid) VALUES ("mitali",33000,2);
